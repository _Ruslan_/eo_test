<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStockUpdateRequest;
use App\Http\Resources\ProductStockResource;
use App\Service\DTO\ProductStockUpdateDto;
use App\Service\ProductStockService;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * @param ProductStockUpdateRequest $request
     * @param ProductStockService $stockService
     * @return mixed
     */
    public function update(ProductStockUpdateRequest $request, ProductStockService $stockService)
    {
        $stockService->updateStock(new ProductStockUpdateDto($request->validated()));

        return response()->json(
            [
                'message' => 'Обновлен успешно'
            ]
        );
    }

    /**
     * @param Request $request
     * @param ProductStockService $stockService
     * @return mixed
     */
    public function getByProductIds(Request $request, ProductStockService $stockService)
    {
        $data = $stockService->getByProductIds($request->get('product_ids'));

        return ProductStockResource::collection($data);
    }

    /**
     * @param Request $request
     * @param ProductStockService $stockService
     * @return mixed
     */
    public function removeByProductIds(Request $request, ProductStockService $stockService)
    {
        $stockService->removeByProductIds($request->get('product_ids'));

        return response()->json(
            [
                'message' => 'Обновлен успешно'
            ]
        );
    }
}
