<?php

declare(strict_types=1);

namespace App\Service\DTO;

class ProductStockUpdateDto
{
    private int $product_id;
    private int $quantity;

    public function __construct(array $data)
    {
        $this->product_id = $data['product_id'];
        $this->quantity   = $data['quantity'];
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->product_id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
