<?php

declare(strict_types=1);

namespace App\Service;

use App\Models\ProductStock;
use App\Service\DTO\ProductStockUpdateDto;

class ProductStockService
{
    public function updateStock(ProductStockUpdateDto $dto)
    {
        ProductStock::updateOrCreate([
            'product_id' => $dto->getProductId(),
        ], [
            'quantity' => $dto->getQuantity(),
        ]);
    }

    /**
     * @param array $productIds
     * @return mixed
     */
    public function getByProductIds(array $productIds)
    {
        return ProductStock::query()
            ->whereIn('product_id', array_values($productIds))
            ->get()
        ;
    }

    /**
     * @param array $productIds
     * @return void
     */
    public function removeByProductIds(array $productIds)
    {
        ProductStock::query()
            ->whereIn('product_id', array_values($productIds))
            ->delete()
        ;
    }
}
